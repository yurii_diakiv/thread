import { Router } from 'express';
import * as userService from '../services/userService';

const router = Router();

router
  .put('/:id', (req, res, next) => userService.updateUser(req.params.id, req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
