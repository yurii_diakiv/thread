import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  SOFT_DELETE_POST,
  SET_UPDATE_POST,
  UPDATE_POST
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SOFT_DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(el => el.id !== action.id)
      };
    case SET_UPDATE_POST:
      return {
        ...state,
        postToUpdate: action.post
      };
    case UPDATE_POST:
      return {
        ...state,
        // posts: state.posts.splice(state.posts.findIndex(el => el.id === action.post.id), 1, action.post)
        posts: state.posts.map(post => (post.id === action.post.id ? action.post : post))
      };
    default:
      return state;
  }
};
