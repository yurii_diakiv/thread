import React, { useState } from 'react';
import Cropper from 'react-easy-crop';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import {
  Button,
  Icon
} from 'semantic-ui-react';
import getCroppedImg from '../../helpers/imageHelper';

const UpdateUserPhoto = ({
  user,
  update: updateUser
}) => {
  const [isUploading, setIsUploading] = useState(false);
  const [originalImage, setOriginalImage] = useState(undefined);

  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [cropSize] = useState({ width: 200, height: 200 });
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);

  const uploadImage = file => imageService.uploadImage(file);

  const onCropChange = _crop => {
    setCrop(_crop);
  };

  const onCropComplete = (croppedArea, _croppedAreaPixels) => {
    // croppedArea;
    setCroppedAreaPixels(_croppedAreaPixels);
  };

  const onZoomChange = _zoom => {
    setZoom(_zoom);
  };

  const openCropper = ({ target }) => {
    const reader = new FileReader();
    reader.readAsDataURL(target.files[0]);
    reader.onload = () => {
      setOriginalImage(reader.result);
    };
  };

  const handleUploadFile = async () => {
    const url = await getCroppedImg(originalImage, croppedAreaPixels);
    const croppedImg = await fetch(url)
      .then(res => res.blob())
      .then(blob => new File([blob], 'profile-photo.jpg', { type: 'image/jpeg' }));

    setIsUploading(true);
    try {
      const { id: imageId } = await uploadImage(croppedImg);
      updateUser(user.id, { imageId });
      setOriginalImage(undefined);
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <>
      { originalImage
        ? (
          <>
            <div style={{ position: 'relative', height: '300px' }}>
              <Cropper
                image={originalImage}
                crop={crop}
                zoom={zoom}
                onCropChange={onCropChange}
                onCropComplete={onCropComplete}
                onZoomChange={onZoomChange}
                cropSize={cropSize}
              />
            </div>
            <br />
            <Button color="green" onClick={handleUploadFile} loading={isUploading} disabled={isUploading}>
              Confirm
            </Button>
          </>
        )
        : (
          <Button color="teal" icon labelPosition="left" as="label">
            <Icon name="image" />
            Change profile photo
            <input name="image" type="file" onChange={openCropper} hidden />
          </Button>
        )}
    </>
  );
};

UpdateUserPhoto.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  update: PropTypes.func.isRequired
};

UpdateUserPhoto.defaultProps = {
  user: {}
};

export default UpdateUserPhoto;
