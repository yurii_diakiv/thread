import { SET_USER, UPDATE_USER } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case UPDATE_USER:
      return {
        ...state,
        user: action.user
      };
    default:
      return state;
  }
};
