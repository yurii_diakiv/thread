import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  toggleExpandedPost,
  sharePost,
  dislikePost,
  softDeletePost,
  userId,
  toggleUpdatePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    postReactions
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
          content={
            postReactions
              ?.filter(reaction => reaction.isLike)
              .map(reaction => (
                <div key={reaction.id}>
                  <Image src={getUserImgLink(reaction.user.image)} avatar />
                  <span>{reaction.user.username}</span>
                </div>
              ))
          }
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleUpdatePost(post)}>
          <Icon name="edit" />
        </Label>
        {user.id === userId && (
          <Label size="small" as="a" className={styles.toolbarBtn} onClick={() => softDeletePost(id)}>
            <Icon name="delete" />
            <Label.Detail>Delete this post</Label.Detail>
          </Label>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  softDeletePost: PropTypes.func,
  userId: PropTypes.string,
  toggleUpdatePost: PropTypes.func.isRequired
};

Post.defaultProps = {
  userId: undefined,
  softDeletePost: undefined
};

export default Post;
