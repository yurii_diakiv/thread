import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import styles from './styles.module.scss';

const UpdateForm = ({
  post,
  updatePost,
  uploadImage,
  toggleUpdatePost
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await updatePost({ id: post.id, imageId: image?.imageId, body });
    toggleUpdatePost();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleUpdatePost}>
        <Form.TextArea
          name="body"
          value={body}
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Update</Button>
      </Form>
    </Segment>
  );
};

UpdateForm.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  toggleUpdatePost: PropTypes.func.isRequired
};

export default UpdateForm;
