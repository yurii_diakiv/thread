import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Popup, Input } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({
  user,
  logout,
  update: updateUser
}) => {
  const [status, setStatus] = useState(user.status ? user.status : '');
  const [edit, setEdit] = useState(false);

  const handleEditStatusClick = () => {
    if (status !== '' && status !== user.status) {
      updateUser(user.id, { status });
      setEdit(!edit);
    }
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI>
                <Image circular src={getUserImgLink(user.image)} />
                <HeaderUI.Content>
                  {user.username}
                  <HeaderUI.Subheader>
                    <Popup
                      content="Press to edit your status"
                      trigger={(
                        <Button
                          basic
                          size="mini"
                          icon="edit outline"
                          style={{ boxShadow: 'none' }}
                          onClick={() => setEdit(!edit)}
                        />
                      )}
                    />
                    {edit
                      ? (
                        <Input
                          action={{ icon: 'check circle outline', onClick: handleEditStatusClick }}
                          size="mini"
                          placeholder="Enter yor status here"
                          type="text"
                          value={status}
                          onChange={ev => setStatus(ev.target.value)}
                        />
                      )
                      : user.status}
                  </HeaderUI.Subheader>
                </HeaderUI.Content>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
