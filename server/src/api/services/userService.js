import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, status, imageId, image } = await userRepository.getUserById(userId);
  return { id, username, email, status, imageId, image };
};

export const updateUser = (id, data) => userRepository.updateById(id, data);
