import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image as SemanticImage,
  Input,
  Button
} from 'semantic-ui-react';
import UpdateUserPhoto from '../../components/UpdateUserPhoto';
import { update } from './actions';

const Profile = ({
  user,
  update: updateUser
}) => {
  const [username, setUsername] = useState(user.username);
  const [status, setStatus] = useState(user.status ? user.status : '');

  const handleEditUserNameClick = () => {
    if (username !== '' && username !== user.username) {
      updateUser(user.id, { username });
    }
  };

  const handleEditStatusClick = () => {
    if (status !== '' && status !== user.status) {
      updateUser(user.id, { status });
    }
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <SemanticImage centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon="info circle"
          iconPosition="left"
          placeholder="Enter yor status here"
          type="text"
          value={status}
          onChange={ev => setStatus(ev.target.value)}
        />
        <Button onClick={handleEditStatusClick}>Edit</Button>
        <br />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          value={username}
          onChange={ev => setUsername(ev.target.value)}
        />
        <Button onClick={handleEditUserNameClick}>Edit</Button>
        <br />
        <br />
        <UpdateUserPhoto user={user} update={updateUser} />
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  update: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  update
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
