import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UpdateForm from 'src/components/UpdateForm';
import { Modal } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import { updatePost, toggleUpdatePost } from '../Thread/actions';

const UpdatePost = ({
  post,
  updatePost: update,
  toggleUpdatePost: toggle
}) => {
  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      <Modal.Content>
        <UpdateForm post={post} updatePost={update} uploadImage={uploadImage} toggleUpdatePost={toggle} />
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  toggleUpdatePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.postToUpdate,
  updatePost: PropTypes.func.isRequired
});

const actions = { updatePost, toggleUpdatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatePost);
